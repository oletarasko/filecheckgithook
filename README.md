Example of git hook.

It checks for added "forbiddenWord" in the files to be committed and interrupts committing if it's found.

Make sure to create soft link to the pre-commit and place it to the .git/hooks. E.g. `ln -s -f ../../pre-commit .git/hooks/pre-commit` from current folder.

Also, make hook executable with chmod +x

#test
